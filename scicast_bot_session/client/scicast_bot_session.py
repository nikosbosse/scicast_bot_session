#!/usr/bin/env python
#
# Copyright (c) 2019 Gold Brand Software, LLC.
# The U.S. Federal Government is granted unlimited rights as defined in FAR 52.227-14.
# All other rights reserved.
#

__version__ = "1.0.7"

import requests
import logging

log = logging.getLogger(__name__)


class SciCastBotSession:
    def __init__(self, base_url, api_key):
        self.session = requests.Session()
        self.base_url = base_url
        self.api_key = api_key

    def close(self):
        pass
        # self.session.close()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def url_get(self, url: str, parameters: dict = None):
        if parameters is None:
            parameters = {}
        parameters['api_key'] = self.api_key
        r = requests.get(self.base_url + "/" + url, parameters)
        if r.status_code != 200:
            raise ValueError(f"Invalid get call to {url} with {r.text}")
        return r.json()

    def url_post(self, url: str, data: dict = None):
        if data is None:
            data = {}
        data['api_key'] = self.api_key

        r = requests.post(self.base_url + "/" + url, data=data)
        if r.status_code != 200:
            raise ValueError(f"Invalid post call to {url} with {data} returns {r.text}")
        return r.json()

    def get_user_info(self):
        return self.url_get("user_info")

    def get_questions(self, round_id: str):
        return self.url_get("questions", {round_id: round_id})

    def get_question_history(self, question_id: int):
        return self.url_get(f"questions/{question_id}/history")

    def get_leaderboard(self, name: str):
        return self.url_get(f"loaderboard/{name}")

    def get_recent_trades(self):
        return self.url_get(f"recent_trades")

    def get_recent_comments(self):
        return self.url_get(f"recent_comments")

    def get_round_info(self, round_id:str = None):
        return self.url_get(f"round_info" if round_id is None else f"round_info?round_id={round_id}")


    def trade(self, question_id: int, new_value: str, max_cost: str = None, comment: str = None, old_value: str = None):
        data = {
            'new_value': new_value,
        }
        if max_cost is not None:
            data['max_cost'] = max_cost
        if comment is not None:
            data['comment'] = comment
        if old_value is not None:
            data['old_value'] = old_value
        return self.url_post(f'trade/{question_id}', data)
