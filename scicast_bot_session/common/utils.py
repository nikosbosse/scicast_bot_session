#!/usr/bin/env python
#
# Copyright (c) 2019 Gold Brand Software, LLC.
# The U.S. Federal Government is granted unlimited rights as defined in FAR 52.227-14.
# All other rights reserved.
#
# common utilities to ease


scicast_bot_urls = {"predict": "https://predict.replicationmarkets.com/bot",
                    "dev": "https://dev.replicationmarkets.com/bot",
                    "covid19": "https://covid19.replicationmarkets.com/bot"}
